//
// Copyright 2014-2018 Amazon.com,
// Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License").
// You may not use this file except in compliance with the
// License. A copy of the License is located at
//
//     http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, express or implied. See the License
// for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import Amplify
import AWSCognitoIdentityProvider

let CognitoIdentityUserPoolRegion: AWSRegionType = .EUWest2
let CognitoIdentityUserPoolId = "eu-west-2_OAwWnUYof"
let CognitoIdentityUserPoolAppClientId = "1dgda3stckm0olkd7pkirbcpul"
let CognitoIdentityUserPoolAppClientSecret = "osbmtqcf1304lmrohc3k33sliggec00cmk7ugkkhps9pjl8bj3p"
let AWSCognitoUserPoolsSignInProviderKey = "CognitoAuth"
